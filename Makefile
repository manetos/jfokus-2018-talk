FILES=index.html \
	  presentation.md \
	  static/remark-latest.min.js \
	  static/YanoneKaffeesatz-Regular.ttf \
	  static/UbuntuMono-Regular.ttf \
	  static/DroidSerif.ttf

IMAGES=\
	media/beta_inside.jpg \
	media/beta_outside.jpg \
	media/Bluetooth.svg \
	media/Z-Wave_logo.jpg \
	media/ublox-xray-closeup.jpg \
	media/ublox-xray-large.jpg \
	media/ublox.jpg \
	media/ublox-hilight.jpg \
	media/ce-testing.jpg \
	media/manetos-all.jpg \
	media/Node.js_logo.svg \
	media/redis-logo.svg \
	media/mqttorg-glow.png \
	media/mosquitto-logo.png \
	media/Debian-OpenLogo.svg \
	media/yocto-project-transp.png \
	media/Jenkins_logo_with_title.png \
	media/Ansible_logo.svg \
	media/grafana-sensors.png \
	media/grafana-logo.svg \
	media/Manetos_logo_red_liten.png \
	media/boiler-picture.jpg \
	media/manetos-open-system.jpg \
	media/CE-logo.svg \
	media/ublox-xray-closeup-airbubbles.jpg \
	media/carrier-board.jpg \
	media/sensor-board.jpg \
	media/smarc.jpg \
	media/refi.jpg \
	media/sensor-flashing.jpg \
	media/sensor-half-assembled.jpg \
	media/sensor-without-front.jpg \
	media/sensor-finished.jpg \


SPECIAL_GRAPHS=\
			   bluetooth-logo-runes.svg

DOT_FILES=$(notdir $(wildcard graphs/*.dot))
GRAPHS_SVG=$(addsuffix .svg, $(addprefix public/media/, $(basename ${DOT_FILES})))

.PHONY: watch
watch:
	while true; do \
		make .reload; \
		inotifywait --quiet --recursive --event close_write --event MOVE_SELF *.md *.html media/* static/* graphs/* Makefile; \
		sleep 0.1; \
	done

public/index.html: public ${FILES} \
	$(addprefix build/, ${SPECIAL_GRAPHS}) \
	${GRAPHS_SVG} \
	${IMAGES} \
	$(addprefix public/, ${IMAGES}) \

	cp build/* public/
	cp --parents ${FILES} public/
	cp index.html public/index.html

public/media/%: public/media media/%
	cp "media/$*" public/media

static public public/media build:
	mkdir -p "$@"

.reload: public/index.html
	./tools/reload.sh
	touch .reload

static/remark-latest.min.js: static
	wget https://remarkjs.com/downloads/remark-latest.min.js -O "$@"
	touch "$@"

static/YanoneKaffeesatz-Regular.ttf: static
	./tools/get-font.sh \
		"https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" \
		"Regular" \
		> "$@"
	touch "$@"

static/DroidSerif.ttf: static
	./tools/get-font.sh \
		"https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic" \
		"Regular" \
		> "$@"
	touch "$@"

static/UbuntuMono-Regular.ttf: static
	./tools/get-font.sh \
		"https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic" \
		"Regular" \
		> "$@"
	touch "$@"

presentation.md:

build/bluetooth-logo-runes.svg: build media/BluetoothLogo.svg graphs/special/bluetooth-logo-runes.dot
	cp media/BluetoothLogo.svg graphs/special/bluetooth-logo-runes.dot build
	cd build && dot "bluetooth-logo-runes.dot" -Tsvg -o "bluetooth-logo-runes.svg"
	./tools/inline-svg.py "$@"

public/media/%.svg: graphs/%.dot public/media
	dot "graphs/$*.dot" -Tsvg -o "public/media/$*.svg"

clean:
	rm -rf public
	rm -rf build
