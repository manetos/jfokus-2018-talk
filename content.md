# Content

## Introduction

Hi, my name is Linus and I work as a developer for Manetos.

[Picture of boiler]

What we do is a smart heating control system. That means that we build a device
that you can connect to your heating system if you live in a single-faminly
house, that is; a villa.

In order to make this work we need a couple of things, first of we need to
connect to the heating system itself. Secondly we need to measure the
temperature, both inside the house and outside.

## Talk overview

[Mention that we exist in mediamarkt and webhallen?]

[Prototype pictures]

This talk is going to be about how we took our product from a basic prototype,
based on a beaglebone, a usb-hub, and some usb-dongles, all the way to a
complete product, that we can sell in a store.

I'm also going to talk about some of the challenges we ran into on the way and
how we tried to solve them.

## Meassuring temperature

The first problem we wanted to solve was how to meassure the temperature, both
inside and out.

In our prototype system we used bluetooth, or more specifically bluetooth low
energy. It worked quite well, except for some issues with drivers and the linux
kernel changing how you deal with bluetooth. All in all it worked quite well.

The big problem was range, we needed to get the temperature from the indoor
sensor, which are most often placed in the living room, down to our system
placed by the boiler in the basement. This was especially problematic in big
houses where the distances are large and in old hoses where you might have very
thick stone floors.

So in order to solve this we want whats called a mesh network

### Mesh networking

#### Star network

In a classic network, like for example classical bluetooth you have what is
called a star network.

This means that you have one central node, in bluetooth that is called the
master, which might be your phone, your laptop or in our case the main unit.
Then you have your peripherals, like your headphones, your keyboard or in our
case, our sensors. All peripherals need to connect directly to the master in
order to be able to participate in the network.

This places obvious limitations on the range of the network as you are limited
by the range of the wireless link.

#### Mesh network

In a mesh network every node in the network can participate in the routing of
messages. This means that for example your headphones doesnt have to be in
close proximity to your laptop but can instead connect via your keyboard. So
with this type of network you can always extend the network by simply adding
more devices.

It also have the benefit of being more resiliant to failure, if one of the
nodes diassapear the network can simply route around it.

### IP Connectivity

The second property we want from our sensor network is IP connectivity. This is
a really nice property which means we can use all of the ordinary tools we are
already accustomed to to talk to the sensors.

For example, we can simply ping the sensors:

[PING code]

We can also simply run a mosquitto server on our systme and simply get messages
via MQTT.

[mosquitto server example]

### Protocols

We looked at four different protocols when we decided on which to go with, they are:

- BLE
- Z-wave
- ZigBee
- 6LoWPAN
    - Thread

#### BLE

The first thing we looked at was bluetooth low energy, which is what we used for our prototypes.

It did not support neither Mesh networking nor IP connectivity.

I do belieave it supports both mesh networking and IP connectivity nowadays, however
I beliave that was just being standardized when we had to take this descision.
This means that we could not use it as there was no hardware nor software availble.

#### Z-Wave

Z-wave does support mesh networking, however it does not support IP connectivity.

Z-wave is mainly used for home automation and if you want to automate your
home, at least here in sweden, it is the way to go.

#### Zigbee

ZigBee biggest products are philips hue and ikea trådfri,

in fact, they can interoperate so you can control your philips bulbs with your
ikea gateway and the other way around

It does support both IP connectivity and mesh networking.

Moreover it is the basis for the 802.15.4 standard. Much like 802.11 defined
how wifi works 802.15.4 defines how sensor network works.

#### 6LoWPAN

6LoWPAN stands for IPv6 over Low-Power Wireless Personal Area Networks and it
basically does what it says on the tin, it provides IPv6 connectivity over
sensor networks. It is based on the 802.15.4 standard, much like ZigBee and
does support both mesh networking and IP connectivity.

Thread for example uses 6LoWPAN

We decided to go with 6LoWPAN because it is based on open standards and
fullfills all of our criterias.

## Software stacks

### Operating systems

We started out with debian on our prototype systems. This worked quite well,
especially with the beaglebones, you can even run upstream debian on them.

However for various reasons we decided to use Yocto for our current versions.
Yocto is technically not an operating system but rather a operating system
building system. It provides tooling for building your own operating system. It
uses what is called recipies, recipies are kind of like a makefile, in it you
define from where to download the package source, how to build it, how to
install it, include postinstall scripts and such, as well as dependencies on
other recipies.

This actually works quite nice, Yocto creates a unique hash for the recipie,
which includes its dependencies. This is how yocto knows if a package needs to
be rebuilt, if any recipie is changed its hash is changed, and it thus needs to
be rebuilt, if the hash hasn't changed then no build is required. This also
includes dependencies, which means that if a package needs to be rebuilt yocto
will also know to rebuild everything that depends on that package.

Yocto also have this tool called the PR service, the PR service takes this hash
and assignes it a version number. Thus if you change any recipie you
automatically get a new version, which is quite neat.

#### Building yocto

The first thing we needed to do with yocto was build it.

##### Jenkins

For building we are using jenkins, we are using jenkins because originally we
needed a ARM slave, something very few other build systems support. Thanks too
yocto this is no longer reqiured though, as it allows us to cross-compile. But
we are still using jenkins.

We want our build slaves and our builds to be transient, this means that they
should not rely on any state on the machine. Basically you should just be able
to clone the repository and run `make` and have everything work.

Yocto however assumes that you have this big build-server running all the time
that everyone in your company uses. This did not match how we wanted to do
things so we had to persist and restore the state of the build system for every
build.

##### sstate-cache

The solution to this is what is called the `sstate-cache`, which is not a cache at all!

This took us quite a while to figure out, in it was only through the help of
the #yocto IRC channel on freenode that we managed to solve it.

Basically what we do is that once a build finishes we simply upload our
`sstate-cache` to s3, then the next time we build we simply download everything
again.

#### checksum failure

This actually worked well, for quite a while. However later we noticed that
some packages in production weren't installable anymore. `apt` was complaining
about a package checksum missmatch. This confused us quite a bit, especially
considering we actually hand't changed production for a bit.

So in order to understand the actual problem we need to understand two things,
the first one being how debian package repositories work and the second being
what yocto actually means with reproducable builds.

This is the directory layout of a debian package repository. For each codename
you have a Release file, in our case we have for codenames:

ci, for automated testing
unstable, for internal manual testing
testing for our beta customers
and stable for our production customers.

Anyway, each codename has a Release file, which is what you get when you run
`apt update`, it contains a bunch of entries like this:

[Release file entry]

This inclues the package name, its version and some other metadata, including
file checksum and the path to the actual debian package, which is part of a
pool of packages.

If we go back to the debian repository layout we will see that the pool is
actually shared between our codenames. This shouldn't actually be a problem,
remember that we have reproducable builds and automatically increasing version
numbers, which should mean anytime we do a change we should get a new package
version, and thus a new entry in the pool.

However this is not really how yocto works with reproducable builds. Yocto has
reproducable builds only to the `sstate`, which we talked about before. The
packaging from the `sstate` to your package format, in our case debian
packages, is not reproducable and non-deterministic.

So what was happening was that when a colleague of mine was testing a new
version of a package in our `ci` environment the build-system also uploaded all
packages to the pool, with different checksums! breaking almost all packages in
our `stable` environment.

The solution, obviously, is simply to not upload all packages but only the ones that have changed.

### Yocto summary

So to summarize, Yocto actually works quite well, however there are some
caveats. My biggest recommendation is to make sure you know how it works and
don't assume to much. I thought I knew how yocto works however those
assumptions have come to bite me in the ass multiple times.

## Monitoring & maintenance

Alright, now we able to get sensor data from our peripherals and we hava an
operating system to run our application on. Lets now look at how we do
monitoring and maintenance.

### Upstreaming data

Any time anything happens on the system, like for example we get a temperature
meassurment, we do a HTTP POST call to our API-server. Our plan is to migrate
this from HTTP to MQTT, mainly because it better matches how we do things
elsewhere, which is in an event-driven style. The other big benefit is the fact
that the overhead per MQTT event is 2 bytes, which is difficult to beat.

### Monitoring

Once an event reaches our API-server we put it onto kafka. From kafka we take
the data and give it to a influx database, which we vizualise with the help of
grafana, and to prometheus where we have alarms.

This setup works very well and I can recommend all three tools, both
Prometheus, InfluxDB and Grafana.

### Maintenance

All maintenance we do on the system we try to do via package upgrades. For
example if we want to do a database migration we prefer to do that in a
post-upgrade script. There are times where we might use ansible to push changes
to our systems, however in general we prefer to do any changes via packages.
Mainly because then the system pulls the changes instead of us having to keep
track and push changes when systems are online.

#### Read-only file system

## Producing systems

So now we have a way of meassuring the temperautre data, getting the
temperature data as well as monitoring and upgrading the system.

Now we need a way of producing our systems

### Flex

When producing hardware what is very common is that you produce your hardware
somewhere in aisa, because it is cheaper. However it is also tougher and can
increase your time to market, especially if you have to visit them often.

Which we ended up doing, a lot. So I'm very thankful that we decided to go with
Flex in Linköping, which ment that whenever I had to go there I only had to
take a 2 hour train-ride instead of a 2 day flight.

It was a bit more expensive but it probably reduced our time to market quite a
bit, so we belieave it was worth it.

### Provisioning

#### Flashing

The first thing we need to do once the system is assembled is to flash it with our operating system.

The main boards in our system we got from embedian, we sent them a tiny image that they flashed onto all the boards before we assembled them.

What this tiny system did was simply to connect to a TFTP server and download our real operating system image and flash it, much like this:

[code for flashing pre-fix]

Which worked fine, however about a week before they needed the final
factory-image we noticed that after adding another package to our image it
failed to flash.

Some of you can probably see the issue already.

Since this is running in initramfs the filesystem is mounted into RAM, and we
only have 512MB of ram of the system, meaning that once the image got bigger
than that we failed to flash.

The fix was easy:

[code of flashing post-fix]

Simply use curl! and pipe directly to the flash

#### Provisioning

Once the final OS image had been flashed the system calls out to a local
jenkins instance to let it know that it is ready for provisioning.

Jenkins then runs an ansible job that gives the system unique identifiers,
secrets. It will test the hardware, register the system in our backend and
print a QR-code containing the unique identifiers.

#### Problematic solders

However when building the first batch of systems our tests failed about 20-25%
of the time, which is obvilusly not very good.

What we could see from the test was that we couldn't communicate with the
GSM-modem or it simply didn't show up as a USB device, as thats how it was
wired internally. Me, being a software guy, was obviously stumped. This is how
the USB-module looks like btw, you can't even see the solders as they are
underneith the module. However Niklas at Flex took one of these broken boards
and ran away. When he came back he showed me a big X-ray machine, obviously
hardware guys have a way of debugging too :)

So this is one of the X-ray pictures:

[X-RAY picture]

So the issue is the big rectangles with the arbitrary circles in them.

The rectangles are the pins coming out of the module, and they them touching
like this is a big problem! that means there is a short. Moreover you can see
bubbles of air in the solder-point as well, which is less than optimal.

I belieave the reason this was a problem was because of the template they used
to apply solder-paste was too large, which meant solder leaked out where it
shouldn't bee.

So the yield of that first batch wasn't very good, we did however for other
reasons move the production of those units to Flex as well, which solved the
problem.

#### CE Certification

The last step to get a system out into the hands of our customers is
CE-certification. It went rather well with the exception of one component, our
ethernet-device, it was generating way too much noise.

Thankfully we only need the ethernet device for provisioning and never again
after that, so simply shutting down the interface solve dthat issue perfectly!

[ip link down thinigie code]

## Summary

Now we are ready! We are ready to sell our system!

## Future
