class: center, middle

# From Beaglebone to Boiler room

## kernel issues and electrical noise

.center[Linus Wallgren]
.center[[linus@manetos.com](mailto:linus@manetos.com)]

![Manetos logo](./media/Manetos_logo_red_liten.png)

.small[https://manetos.gitlab.io/jfokus-2018-talk]

--

---
class: middle

![Manetos system](./media/manetos-all.jpg)

???

- Smart heating control system
- Device for single family houses
- 15 people
    - ½ devs

---
class: center

![Picture of heating system](./media/boiler-picture.jpg)

???

- Connect heating system
- Temperature
    - In
    - Out

---
class: center, middle

![β outside](./media/beta_outside.jpg)

???

# β → ✓

- Scypho?

---
class: center, middle

![β inside](./media/beta_inside.jpg)

---
class: middle

![Manetos system](./media/manetos-all.jpg)

---

# Outline

.todo[
- Measuring temperatures
- Software stacks
- Monitoring and maintenance
- Producing systems
- Certification
]

---
class: center, middle
# Measuring temperature

???

- Indoor
- Outdoor
- prototype
    - BLE
    - Driver & kernel
    - range
        - We chose mesh

---
class: center, middle

# Star network

![star network](./media/topology-star.svg)

## Classic bluetooth

???

# Classical network

- Star topology
- Bluetooth: master
- Limited to medium

---
class: middle, mesh-network
.column[
#Mesh network

## Modern Bluetooth low energy
]
.column[
![star network](./media/topology-mesh.svg)
]

???

# Mesh network

- Mesh topology
- Everyone routes
- More resiliant

---

## IP Connectivity

    $ ping6 --count 10 aaaa::2bba

???

Ordinary tools

---
animate: [73, 1076, 2322, 1822, 1070, 1072, 1070, 1070, 1192, 1133]
animationId: da5cef3e-03c1-11e8-a2ee-e7d160393f36
count: false

## IP Connectivity

    $ ping6 --count 10 aaaa::2bba
    PING aaaa::2bba (aaaa::2bba): 56 data bytes
--
    64 bytes from aaaa::2bba: icmp_seq=0 ttl=62 time=73.388 ms
--
    64 bytes from aaaa::2bba: icmp_seq=1 ttl=62 time=76.599 ms
--
    64 bytes from aaaa::2bba: icmp_seq=2 ttl=62 time=1322.674 ms
--
    64 bytes from aaaa::2bba: icmp_seq=3 ttl=62 time=822.204 ms
--
    64 bytes from aaaa::2bba: icmp_seq=4 ttl=62 time=70.663 ms
--
    64 bytes from aaaa::2bba: icmp_seq=5 ttl=62 time=72.757 ms
--
    64 bytes from aaaa::2bba: icmp_seq=6 ttl=62 time=70.931 ms
--
    64 bytes from aaaa::2bba: icmp_seq=7 ttl=62 time=70.797 ms
--
    64 bytes from aaaa::2bba: icmp_seq=8 ttl=62 time=192.370 ms
--
    64 bytes from aaaa::2bba: icmp_seq=9 ttl=62 time=133.737 ms
    --- aaaa::2bba ping statistics ---
    10 packets transmitted, 10 packets received, 0% packet loss
    round-trip min/avg/max/stddev = 70.663/290.612/1322.674/408.339 ms

---

```
$ mosquitto_sub --topic 'iot-1/id/+' | jq .
```

???

- How we get sensor data
- MQTT - Event driven message bus
    - Sensor → MQTT → application
- From test-system @ office

---
animate: [583, 5000, 123]
animationId: 40eecc88-03c5-11e8-967c-5b1fb6ae8954
count: false
```
$ mosquitto_sub --topic 'iot-1/id/+' | jq .
```

--
animate: [5000, 123]
animationId: 40eecc88-03c5-11e8-967c-5b1fb6ae8954
count: false

```json
{
  "ambientTemp": 19500,
  "defRoute": "fe80::ea0d",
  "rssi": -84,
  "vdd": 2252
}
```

---
animate: [123]
animationId: 40eecc88-03c5-11e8-967c-5b1fb6ae8954
count: false
```
$ mosquitto_sub --topic 'iot-1/id/+' | jq .
```
```json
{
  "ambientTemp": 19500,
  "defRoute": "fe80::ea0d",
  "rssi": -84,
  "vdd": 2252
}
{
  "ambientTemp": 23300,
  "defRoute": "fe80::d225",
  "rssi": -57,
  "vdd": 2528
}
```

---
animationId: 40eecc88-03c5-11e8-967c-5b1fb6ae8954
count: false
```
$ mosquitto_sub --topic 'iot-1/id/+' | jq .
```
```json
{
  "ambientTemp": 19500,
  "defRoute": "fe80::ea0d",
  "rssi": -84,
  "vdd": 2252
}
{
  "ambientTemp": 23300,
  "defRoute": "fe80::d225",
  "rssi": -57,
  "vdd": 2528
}
{
  "ambientTemp": 19500,
  "defRoute": "fe80::ea0d",
  "rssi": -85,
  "vdd": 2251
}
```

---

# Communication protocols

- Bluetooth Low Energy
- Z-Wave
- ZigBee
- 6LoWPAN

---
class: bluetooth

![Bluetooth logo](./media/Bluetooth.svg)

- All the phones
- No Mesh networking (at the time)
- No IP Connectivity (at the time)

???

[Harald Blåtand logo](./media/bluetooth-logo-runes.svg)

---

![Z-Wave logo](./media/Z-Wave_logo.jpg)

- Home automation
- Mesh networking
- No IP Connectivity

---

# ZigBee

- IP Connectivity
- Mesh networking
- IEEE 802.15.4

???

- Philips hue
- IKEA trådfri
- Interoperability
- 802.15.4 <-> 802.11

---

# 6LoWPAN

IPv6 over Low-Power Wireless Personal Area Network

- IP Connectivity
- Mesh networking
- IEEE 802.15.4

???

- IPv6 over Low-Power Wireless Personal Area Network
- Thread
- Our descicion
    - Open standards
    - Fullfills our criteria

---

# Outline

.completed[
- Measuring temperatures
].todo[
- Software stacks
- Monitoring and maintenance
- Producing systems
- Certification
]

???

Elapsed: 7:40

---
class: center, middle

# Software stacks

---
class: slide-application, center

# Main unit application

.pad-right[![nodejs](./media/Node.js_logo.svg)]
![redis](./media/redis-logo.svg)

.pad-right[![mqtt](./media/mqttorg-glow.png)]
![mosquitto](./media/mosquitto-logo.png)

???

- prototype works on prod
- probably redis -> sqlite

---
class: center, middle, slide-os

![Debian](./media/Debian-OpenLogo.svg)
# ↓
![Yocto](./media/yocto-project-transp.png)

???

- OS
- (with debian tooling)
- Not a distro
- Distro building distro

---
class: maximize, center, middle

![Yocto IO](./media/yocto-io.svg)

???

- Recipies
    - → (images & deb packages)

---

```
SUMMARY = "Software watchdog"
LICENSE = "GPL-2.0+"

RDEPENDS_${PN} = "python3"

SRC_URI = "${SOURCEFORGE_MIRROR}/watchdog/watchdog-${PV}.tar.gz \
           file://watchdog.conf \
           file://software-watchdog.service"

SRC_URI[md5sum] = "678c32f6f35a0492c9c1b76b4aa88828"

inherit autotools
inherit systemd

SYSTEMD_SERVICE_${PN} = "software-watchdog.service"

do_install_append() {
    install -d ${D}/etc/
    install -m 0644 ${WORKDIR}/watchdog.conf ${D}/etc/
}
```

???

- __Brief__ recipie description
- Hashes input metadata
- PR Service
- Reproducable builds
    - Very slow - 4h

---

# Building yocto

![Jenkins](./media/Jenkins_logo_with_title.png)

???

- Jenkins: ARM
- Transient slaves/builds:
    - Machines dies
    - Replace them
    - Run anywhere
- Yoctos big server
- Flash once, don't touch

---

# Building yocto - transient builds
## sstate-cache

???

- originally: speed
    - shared state cache
    - clean build takes 4h
- rburton #yocto@freenode

--

- _not really_ a cache

---

# Building yocto - transient builds
## sstate-cache

1. Download sstate from s3
2. Build
3. Upload sstate to s3

---
class: maximize
# Building yocto

![build and upload](./media/build-upload.svg)

???

- Rebuild @ commit
- Upload images & __deb__ packages

- Any change triggers build
    - New packages -> deb-repo
    - New images -> s3
- Worked well until...

---

# Building yocto - Hash Sum mismatch

```
host:~# apt install netris
…
Get: 1 http://repo.manetos.test/ stable/main netris amd64
0.52-10+b1 [36 kB]
Fetched 36 kB in 1s (121 kB/s)

E: Failed to fetch
http://repo.manetos.test/pool/main/n/netris/netris_0.52-10+b1_amd64.deb:
Hash Sum mismatch
```

???

- Thought we were done
- Untouched prod
- Started investigating deb repo

---

# Building yocto - Hash Sum mismatch
## Deb repository

```
debian
├── dists
│   ├── ci/main/binary-armhf
│   │   └── Packages
│   └── stable/main/binary-armhf
│       └── Packages
└── pool/main/n/netris
    └── netris_0.52-10+b1_amd64.deb
```

???

- ci
- unstable
- testing
- stable

---

## debian/dists/ci/main/binary-armhf/Packages

```
…
Package: netris
Source: netris (0.52-10)
Version: 0.52-10+b1
Architecture: amd64
Depends: libc6 (>= 2.14), libncurses5 (>= 6), libtinfo5 (>= 6)
Description: free, networked version of T*tris
Homepage: http://netris.org/
Section: games
Priority: optional
Filename: pool/main/n/netris/netris_0.52-10+b1_amd64.deb
Size: 36868
MD5sum: 9f18de12e7d0f5e1d9f2515754adef0f
SHA256: 3a8af04f30300f8a1cc458e015e46681a5ff2f0806f6c4e83ba58fa4017d54de
…
```

???

- `apt update`

---
count: false

## debian/dists/ci/main/binary-armhf/Packages

```
…
Package: netris
Source: netris (0.52-10)
Version: 0.52-10+b1
Architecture: amd64
Depends: libc6 (>= 2.14), libncurses5 (>= 6), libtinfo5 (>= 6)
Description: free, networked version of T*tris
Homepage: http://netris.org/
Section: games
Priority: optional
*Filename: pool/main/n/netris/netris_0.52-10+b1_amd64.deb
Size: 36868
MD5sum: 9f18de12e7d0f5e1d9f2515754adef0f
SHA256: 3a8af04f30300f8a1cc458e015e46681a5ff2f0806f6c4e83ba58fa4017d54de
…
```

---

# Building yocto - Hash Sum mismatch
## Deb repository

```
debian
├── dists
│   ├── ci/main/binary-armhf
│   │   └── Packages
│   └── stable/main/binary-armhf
│       └── Packages
└── pool/main/n/netris
*    └── netris_0.52-10+b1_amd64.deb
```
???

- Not a problem, given:
    - Reproducable builds
    - Automatically increasing versions

- Yocto
    - Reproducable _builds_
    - NOT packaging

---
count:false
# Building yocto - Hash Sum mismatch
## Deb repository

```
debian
├── dists
│   ├── ci/main/binary-armhf
│   │   └── Packages                          …8ae4944c
│   └── stable/main/binary-armhf
│       └── Packages                          …8ae4944c
└── pool/main/n/netris
    └── netris_0.52-10+b1_amd64.deb           …8ae4944c
```
???

- Before
- Testing in ci -> breaking stable

---
count:false
# Building yocto - Hash Sum mismatch
## Deb repository

```
debian
├── dists
│   ├── ci/main/binary-armhf
│   │   └── Packages                          …017d54de
│   └── stable/main/binary-armhf
*│       └── Packages                          …8ae4944c
└── pool/main/n/netris
    └── netris_0.52-10+b1_amd64.deb           …017d54de
```
???
- Testing in ci -> breaking stable

- Only upload changes

---
class: center, middle

# TL;DL.
# Yocto

???

- Neat
- Caveats!
- Don't assume too much!

---

# Outline

.completed[
- Measuring temperatures
- Software stacks
].todo[
- Monitoring and maintenance
- Producing systems
- Certification
]

???

Elapsed: 18:45

---
class: center, middle

# Monitoring and maintenance

---
class: center, middle

# Upstreaming data
## HTTP → MQTT

???

- 2 bytes overhead
- Event driven
    - Uniform architecture

---
# Monitoring: architecture

![monitoring dataflow](./media/monitoring-dataflow.svg)

???

- Arch for monitoring
- kafka
    - Message bus
    - Event driven
- Recommend

---
![grafana logo](./media/grafana-logo.svg)

![grafana dashboard](./media/grafana-sensors.png)

???

- Sensor data
- Battery
- Temperature

---
# Upgrades
## Continuous delivery

| Stage    | Env     | Location    |
| ---      | ---     | ---         |
| pre-α    | jenkins | AWS         |
| ci       | α       | office      |
| unstable | iβ      | office      |
| testing  | eβ      | β-customers |
| stable   | prod    | customers   |

???

| Stage    | Description               |
| ---      | ---                       |
| pre-α    | Unit tests                |
| ci       | Automated on systems      |
| unstable | Manual on systems         |
| testing  | Testing on beta-customers |
| stable   | Released                  |

- Feature flags

---
# Maintenance

## Packages

- apt/dpkg
- Handles offline systems

## Ansible

- Easy to write
- Idempotent

???

- Reuse tooling
- Offline systems

---

# Outline

.completed[
- Measuring temperatures
- Software stacks
- Monitoring and maintenance
].todo[
- Producing systems
- Certification
]

???

Elapsed: 24:00

---
class: center, middle

# Producing systems

???

- What/why "production line"?
- Flex vs asia
    - More expensive
    - Shorter time to market
    - Faster iterations - Learn quicker
    - Less painful: 2h vs 20h

---
class: slide-prodtools

# Production-line as code

.pad-right[![Jenkins](./media/Jenkins_logo_with_title.png)]
![Ansible](./media/Ansible_logo.svg)

# [Jenkins job builder](https://docs.openstack.org/infra/jenkins-job-builder/)

???

- No info about how to do this
- Reuse tools

- Reproducable & idempotent
- Infrastructure as code → Production-line as code
    - Easy to test
    - Fast to fix
    - Reduces risk
    - Documentation
- Office

- jenkins
    - "Friendly" UI
- ansible
    - Idempotency

---

# Production-line as code

1. Configures machines
    - Jenkins
    - DHCP
    - TFTP
    - Printer drivers
2. Configure jenkins
    - Jenkins job builder

???

Ansible → ( Jenkins & JJB ) → Ansible → System

---
class: slide-hardware, center, middle

![Carrier board](./media/carrier-board.jpg)
![SMARC Module](./media/smarc.jpg)

![Custom hardware](./media/refi.jpg)
![Sensor board](./media/sensor-board.jpg)

???

- Carrier board
    - 100 first from aisa
    - GSM
    - 6LoWPAN
- SMARC
    - Embedian
    - BBB Compatible
- Custom hardware
    - USB

---
class: slide-hardware

# Sensors

![Sensor half assembled](./media/sensor-half-assembled.jpg)
![Sensor without front](./media/sensor-without-front.jpg)
![Sensor finished](./media/sensor-finished.jpg)

???

1. Assemble
2. Flash
3. Get QR-code
5. Assemble
6. Test
7. Assemble


---
class: maximize
![production flow](./media/production-flow.svg)

???

- super-simple factory-image to Embedian
- TFTPs real image
- Failed a week before final version

---
# Flashing

## Borken
```bash
#!/bin/sh
tftp -g -r rootfs.ext4 192.168.88.1
dd if=./rootfs.ext4 of=/dev/mmcblk0p2
```

--

## Working

```sh
#!/bin/sh
curl -s --tftp-blksize 2048 \
    tftp://192.168.88.1/rootfs.ext4 \
    | dd of=/dev/mmcblk0p2 bs=2048
```

---

# Main unit

1. Main unit notifies jenkins
--

2. jenkins runs ansible
--

    1. Create unit-specific identifiers
--
    2. Install secrets
--
    3. Test hardware
--
    4. Register unit
--
    5. Print QR-code
--
    6. Disable notification job

---
class: center, middle

# First batch

--

## 20% GSM Failure

???

- Couldnt talk to modem
- Missing from `lsusb`

---

![ublox overview](./media/ublox.jpg)

---
count:false
![ublox overview circled](./media/ublox-hilight.jpg)

???

- SoM - System on module
- Niclas ran away

---
class: center, maximize

![ublox xray overview](./media/ublox-xray-large.jpg)

???

- Hardware debugging
- X-ray!

---
class: center

![ublox xray overview](./media/ublox-xray-closeup.jpg)

???

- Niclas pointing
- short!
- Too large template for solder paste

---
class: center
count:false
![ublox xray airbubbles](./media/ublox-xray-closeup-airbubbles.jpg)

???
- Air-bubbles
- Moving to flex solved issue

---

# Outline

.completed[
- Measuring temperatures
- Software stacks
- Monitoring and maintenance
- Producing systems
].todo[
- Certification
]

???

Elapsed: 37:00

---
class: middle, center

![CE](./media/CE-logo.svg)

# Certification

???

- CE -> Requirements to sell

---

# .ce[![CE](./media/CE-logo.svg)] Certification

![ce certification](./media/ce-testing.jpg)

???

- How noisy are we?
- How affected are we by noise?
- Temperature?

---
# .ce[![CE](./media/CE-logo.svg)] Certification

## Noisy ethernet device

--

```rust
host:~# ip link set eth0 down
```

---

# Outline

.completed[
- Measuring temperatures
- Software stacks
- Monitoring and maintenance
- Producing systems
- Certification
]

???

Elapsed: 39:30

---
# Root file system read-only

```
host:~$ touch foo
touch: cannot touch 'foo': Read-only file system
```
--
- Installing applications
--

- ✓ Reboot


---
count:false
# Root file system corruption

```
host:~$ touch foo
touch: cannot touch 'foo': Read-only file system
```

- Installing applications
- ✓ Reboot
- ☠ Filesystem

---

# Root file system corruption

    blk_update_request: I/O error, dev mmcblk0

???

- Bad flash?!

---
# Root file system corruption

```
*mmcqd/1: page allocation failure: order:4

edma edma_prep_slave_sg: Failed to allocate a descriptor
omap_hsmmc 481d8000.mmc: prep_slave_sg() failed
omap_hsmmc 481d8000.mmc: MMC start dma failure

mmcblk0: unknown error -1 sending read/write command
blk_update_request: I/O error, dev mmcblk0
⋮
Aborting journal on device mmcblk0p2-8.

EXT4-fs error (mmcblk0p2): Detected aborted journal
EXT4-fs (mmcblk0p2): Remounting filesystem read-only
```


???

mmcpd Failed an order 4 allocation

---

# Root file system corruption

```
mmcqd/1: page allocation failure: order:4

*edma edma_prep_slave_sg: Failed to allocate a descriptor
*omap_hsmmc 481d8000.mmc: prep_slave_sg() failed
*omap_hsmmc 481d8000.mmc: MMC start dma failure

mmcblk0: unknown error -1 sending read/write command
blk_update_request: I/O error, dev mmcblk0
⋮
Aborting journal on device mmcblk0p2-8.

EXT4-fs error (mmcblk0p2): Detected aborted journal
EXT4-fs (mmcblk0p2): Remounting filesystem read-only
```


???

`edma` and `omap_hsmcc` bubbles error

---
count:false
# Root file system corruption

```
mmcqd/1: page allocation failure: order:4

edma edma_prep_slave_sg: Failed to allocate a descriptor
omap_hsmmc 481d8000.mmc: prep_slave_sg() failed
omap_hsmmc 481d8000.mmc: MMC start dma failure

*mmcblk0: unknown error -1 sending read/write command
*blk_update_request: I/O error, dev mmcblk0
⋮
Aborting journal on device mmcblk0p2-8.

EXT4-fs error (mmcblk0p2): Detected aborted journal
EXT4-fs (mmcblk0p2): Remounting filesystem read-only
```

???

- `mmcblk0` doesn't understand
- `mmcblk0`: failed write?
- repeated

---
count:false
# Root file system corruption

```
mmcqd/1: page allocation failure: order:4

edma edma_prep_slave_sg: Failed to allocate a descriptor
omap_hsmmc 481d8000.mmc: prep_slave_sg() failed
omap_hsmmc 481d8000.mmc: MMC start dma failure

mmcblk0: unknown error -1 sending read/write command
blk_update_request: I/O error, dev mmcblk0
⋮
*Aborting journal on device mmcblk0p2-8.

EXT4-fs error (mmcblk0p2): Detected aborted journal
EXT4-fs (mmcblk0p2): Remounting filesystem read-only
```

???
- Aborted filesystem journal

---
count:false
# Root file system corruption

```
mmcqd/1: page allocation failure: order:4

edma edma_prep_slave_sg: Failed to allocate a descriptor
omap_hsmmc 481d8000.mmc: prep_slave_sg() failed
omap_hsmmc 481d8000.mmc: MMC start dma failure

mmcblk0: unknown error -1 sending read/write command
blk_update_request: I/O error, dev mmcblk0
⋮
Aborting journal on device mmcblk0p2-8.

*EXT4-fs error (mmcblk0p2): Detected aborted journal
*EXT4-fs (mmcblk0p2): Remounting filesystem read-only
```

???
- Read-only

- Not bad flash
- Memory fragmentation?

---
# Root file system corruption
## Memory fragmentation?

    mmcqd/1: page allocation failure: order:4
---

# Root file system corruption
## Memory fragmentation?

    2⁴ * 4096 B = 16 * 4096 B = 65536 B = 64 kiB
---
# Root file system corruption
## Memory fragmentation?

```
Normal:
    2387*4kB (MEC)
    2147*8kB (UMC)
    545*16kB (UMEC)
    160*32kB (C)
*   54*64kB (C)
    4*128kB (C)
    4*256kB (C)
    0*512kB
    0*1024kB
    0*2048kB
    0*4096kB
    0*8192kB
    = 45556kB
```

???

- No fragmentation :(

---

# Root file system corruption
## Memory issue?

    mmcqd/1: page allocation failure: order:4

???

- Only @ memory load?

---
# Root file system corruption
## Memory issue?

```c
const int MB = 1024 * 1024;
const int len = 450 * MB;

char* buf = (char*)malloc(len);

for (int i = 0; i < len; ++i) {
    buf[i] = 1;
}
```

???

- Reliably crash it

---
count: false
# Root file system corruption
## Memory issue!

```c
const int MB = 1024 * 1024;
const int len = 450 * MB;

char* buf = (char*)malloc(len);

for (int i = 0; i < len; ++i) {
    buf[i] = 1;
}
```

???

- Kill memory users?

---
# Root file system corruption
## Memory issue!

## Before

```rust
host:~# sysctl vm.min_free_kbytes
vm.min_free_kbytes = 2785
```

--

## After

```rust
host:~# sysctl vm.min_free_kbytes=16384
```

???

- Increase atomic memory in kernel
    - Non-blocking
    - Interupt handlers
- Multiple things to keep min_free_kbytes:
    - OOM Killer
    - dead processes > corruption
- Only a workaround
- package-upgrade


---
class: center, middle

# From Beaglebone to Boiler room

## kernel issues and electrical noise

.center[Linus Wallgren]
.center[[linus@manetos.com](mailto:linus@manetos.com)]

![Manetos logo](./media/Manetos_logo_red_liten.png)

.small[https://manetos.gitlab.io/jfokus-2018-talk]

???

- Our system is now being sold
- Expect challenges
- We still have many more to come
    - Like next-gen
        - 4.4 -> 3.17
