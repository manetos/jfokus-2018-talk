#!/bin/bash

set -o pipefail
set -e
set -u

font_url=$(curl -s "$1" | grep "$2" | grep -Po 'url\(\K[^)]+')
curl -s "$font_url"
