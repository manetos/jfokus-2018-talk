#!/usr/bin/python3

import xml.etree.ElementTree as ET
import os.path as path
import base64
import sys

if len(sys.argv) != 2:
    sys.exit(1)

svg_input_file = sys.argv[1]

tree = ET.parse(svg_input_file)

def find_images(node):
    if node.tag.endswith('image'):
        yield node
    for child in node:
        yield from find_images(child)

def get_base64(path):
    with open(path, "rb") as image_file:
        return base64.b64encode(image_file.read())

for image in find_images(tree.getroot()):
    image_path = path.join(
            path.dirname(svg_input_file),
            image.attrib['{http://www.w3.org/1999/xlink}href']
            )

    encoded = get_base64(image_path).decode('utf8')
    image.set('{http://www.w3.org/1999/xlink}href', 'data:image/svg+xml;base64,%s' % encoded)

tree.write(sys.argv[1])
